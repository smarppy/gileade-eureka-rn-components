import React, { PropTypes, Component } from 'react';
import {
  FormLabel,
  Button
} from 'react-native-elements'
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Form from '../form';
import Username from './username';
import Password from './password';

const MsgValidation = ({ style, message }) => (
  <Text style={[
    style,
    {
      position: 'absolute',
      left: 20,
      top: -10
    }
  ]}>
    {message}
  </Text>
);

export default class Login extends Component {

  static propTypes = {
    onSubmit: PropTypes.func,
    light: PropTypes.bool,
    validationMessage: PropTypes.string
  }

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    };
  }

  getLabels = () => {
    const { labels = {} } = this.props;
    const defaults = {
      submit: 'Entrar',
      username: 'E-mail',
      password: 'Senha',
      forgetPassword: 'Esqueci minha Senha',
      passwordLenghtValidation: 'Senha dever ter 4 ou mais digitos'
    };
    return { ...defaults, ...labels };
  }

  getCustomStyles() {
    const { customStyles = {} } = this.props;
    return {
      messageValidationColor: '#c4c4c4',
      text: '#ffffff',
      ...customStyles
    };
  }

  renderSubmit = (submit) => {
    const { customStyles = {} } = this.props;
    const labels = this.getLabels();
    return (
      <Button
        Component={TouchableOpacity}
        onPress={submit}
        containerViewStyle={{ marginTop: 15 }}
        buttonStyle={customStyles.submit}
        raised
        icon={{ name: 'check' }}
        title={labels.submit} />
    );
  }

  handleSubmit = () => {
    const { onSubmit = () => { } } = this.props;
    const { username, password } = this.state;
    onSubmit(username, password);
  }

  renderFormFields = () => {
    const { style, validationMessage } = this.props;
    const customStyles = this.getCustomStyles();
    const labels = this.getLabels();
    const { username, password } = this.state;

    return (
      <Form style={style}
        onSuccess={this.handleSubmit}
        submitButton={this.renderSubmit}>
        {validationMessage ? (
          <MsgValidation
            style={{ color: customStyles.messageValidationColor }}
            message={validationMessage} />
        ) : null}
        <Username
          label={labels.username}
          value={username}
          onChangeText={username => this.setState({ username })}
          customStyles={customStyles} />
        <Password
          labels={labels}
          value={password}
          customStyles={customStyles}
          onChangeText={password => this.setState({ password })} />
      </Form>
    );
  }

  render() {
    return (
      <View>
        {this.renderFormFields()}
        {this.props.socialButtons}
      </View>
    );
  }
}
